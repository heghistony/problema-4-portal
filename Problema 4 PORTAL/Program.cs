﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4Probleme
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Preluarea numarului <<N>>: ");
            int n = int.Parse(Console.ReadLine());
            int[] v = new int[n];
            Console.WriteLine("Introduceti elementele vectorului <<N>>: ");
            for (int i = 0; i < n; i++)
            {
                v[i] = int.Parse(Console.ReadLine());
            }
            int suma = 0;
            for (int i = 0; i < v.Length - 1; i += 2)
            {
                suma = v[i] + v[i + 1];
                Array.Resize(ref v, v.Length + 1);
                for (int j = v.Length - 1; j > i + 1; j--)
                {
                    v[j] = v[j - 1];
                }
                v[i + 1] = suma;
            }

            for (int i = 0; i < v.Length; i++)
            {
                Console.Write(v[i]);
            }


            Console.ReadKey();

        }
    }
}
